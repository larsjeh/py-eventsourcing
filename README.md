# Event-sourcing
Inspired by the [Broadway](https://github.com/broadway/broadway/blob/master/README.md) CQRS/ES framework

![CQRS image](docs/CQRS.jpg)
## Installation
```
$ pip install py-eventsourcing
```

## Components
- [EventBus]()
- [CommandBus]()
- [CommandHandler]()
- [Command]()
- [EventHandler]()
- [Event]()
- [AggregateRoot]()
- [EventSourcingRepository]()
- [EventStore]()

## Examples
Examples can be found in the examples/ directory. Most of the examples focus on showing how one of the components works. There is also a more deliberate example using several components.

## Conventions

### Upcoming 
- Support for event sourced entities nested within the aggregate root
- Unit tests
