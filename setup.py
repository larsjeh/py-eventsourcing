from setuptools import setup, find_packages

try: # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError: # for pip <= 9.0.3
    from pip.req import parse_requirements

# parse_requirements() returns generator of pip.req.InstallRequirement objects
install_reqs = parse_requirements('requirements.txt',session=False)

# reqs is a list of requirement
reqs = [str(ir.req) for ir in install_reqs]


setup(name='py-event-sourcing',
      version='0.1',
      license='MIT',
      packages=find_packages(),
      install_requires = reqs,
      zip_safe=False)