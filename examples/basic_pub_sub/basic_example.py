
from eventsourcing.AbstractEvent import AbstractEvent
from eventsourcing.eventbus.in_memory import InMemoryEventBus

from eventsourcing.listener.in_memory import InMemoryEventListener


class UserIsCreatedEvent(AbstractEvent):
    def __init__(self, name):
        self.name = name

    @property
    def version(self):
        return 1

class PrintEventListener(InMemoryEventListener):
    def handle_user_is_created_event(self, event: UserIsCreatedEvent):
        print("Hello {name}".format(name=event.name))


if __name__ == '__main__':
    bus = InMemoryEventBus()

    console_event_listener = PrintEventListener()
    bus.subscribe(console_event_listener)

    event = UserIsCreatedEvent(name='Lars')

    bus.publish(event)

