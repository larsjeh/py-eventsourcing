import uuid
import boto3

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from eventsourcing.abstract import EventSourcingRepository
from eventsourcing.in_memory import InMemoryEventBus
from eventsourcing.in_memory import InMemoryCommandBus
from eventsourcing.eventstore.sqlalchemy import SqlAlchemyEventstore
from eventsourcing.eventstore.entity import Base

from user_command_handler import UserCommandHandler
from user import User
from eventsourcing.kinesis import KinesisEventPublisher, KinesisProducer
from events import UserIsCreatedSchema, UserIsRemovedSchema
from commands import CreateUserCommand
from eventsourcing.serializer.marshmallow import MarshmallowJSONSerializer
from marshmallow_oneofschema import OneOfSchema
import time


class UserEventSchemas(OneOfSchema):
    type_field = "messageType"
    type_schemas = {
        'UserIsCreatedEvent': UserIsCreatedSchema,
        'UserIsRemovedEvent': UserIsRemovedSchema
    }

if __name__ == '__main__':
    # Create in-memory eventbus
    event_bus = InMemoryEventBus()


    # add kinesis listener which publishes all events to kinesis
    kinesis_client = boto3.client("kinesis", endpoint_url='http://localhost:4567',aws_access_key_id="dev",
                                      aws_secret_access_key="dev")
    producer = KinesisProducer(stream_name='eventsourcing-test-stream', batch_size=100, kinesis_client=kinesis_client, batch_time=5)
    serializer = MarshmallowJSONSerializer(UserEventSchemas)
    kinesis_events_producer = KinesisEventPublisher(producer, serializer)
    event_bus.subscribe(kinesis_events_producer)

    # Create necessary db connection to configure a mysql event store
    engine = create_engine('mysql://root:eventstore@127.0.0.1:3306/eventstore')
    Base.metadata.create_all(engine)
    session = sessionmaker(bind=engine)
    session.configure(bind=engine)
    event_store = SqlAlchemyEventstore(session())

    # create command handler and subscribe it to the command rabbitmq_bus
    eventsourced_repository = EventSourcingRepository(event_store, event_bus, User)
    user_command_handler = UserCommandHandler(eventsourced_repository)

    command_bus = InMemoryCommandBus()
    command_bus.subscribe(user_command_handler)

    # wait until stream is created
    time.sleep(2)
    command_bus.publish(CreateUserCommand(username='Lars'))