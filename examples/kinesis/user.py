from eventsourcing.abstract import AggregateRoot
from events import UserIsRemovedEvent, UserIsCreatedEvent
class User(AggregateRoot):
    @staticmethod
    def create(user_id, name):
        """
        Spawns a new user with a given user_id and name
        :param user_id: str
        :param name: str
        :return: User
        """
        user = User()
        user.apply(UserIsCreatedEvent(name=name, user_id=user_id))

        return user

    @property
    def aggregate_id(self):
        """
        Mandatory function to determine the id of the aggregate
        :return:
        """
        return self.user_id


    def apply_user_is_created_event(self, event: UserIsCreatedEvent):
        """
        Set the correct properties for the newly created user
        :param event: UserIsCreatedEvent
        :return:
        """
        self.name = event.name
        self.user_id = event.user_id