from eventsourcing.abstract import Command
class CreateUserCommand(Command):
    def __init__(self, username: str):
        self.username = username

