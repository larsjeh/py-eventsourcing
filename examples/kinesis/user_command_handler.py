from eventsourcing.abstract import EventSourcingRepository
from eventsourcing.in_memory import InMemoryCommandHandler
from commands import CreateUserCommand
from user import User
import uuid

class UserCommandHandler(InMemoryCommandHandler):
    def __init__(self, repository: EventSourcingRepository):
        # create event sourcing repository containing event store and event rabbitmq_bus
        self._repository = repository


    def handle_create_user_command(self, command: CreateUserCommand):
        """
        Handles the create user command
        :param command:
        :return:
        """
        user_id = uuid.uuid4()
        print(str(user_id))
        user = User.create(str(user_id), command.username)

        # save aggregate to repository
        self._repository.save(user)

