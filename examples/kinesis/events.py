from eventsourcing.abstract import BaseEvent
from marshmallow import Schema, fields, post_load

class UserIsCreatedEvent(BaseEvent):
    def __init__(self, name, user_id):
        self.name = name
        self.user_id = user_id

class UserIsRemovedEvent(BaseEvent):
    def __init__(self, user_id):
        self.user_id = user_id


class UserIsRemovedSchema(Schema):
    user_id = fields.Str()

    @post_load
    def cast(self, data):
        return UserIsRemovedEvent(**data)

class UserIsCreatedSchema(Schema):
    name = fields.Str()

    @post_load
    def cast(self, data):
        return UserIsCreatedEvent(**data)
