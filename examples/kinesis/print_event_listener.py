from eventsourcing.in_memory import InMemoryEventListener
from events import UserIsCreatedEvent, UserIsRemovedEvent, UserChangedNameEvent

class PrintEventListener(InMemoryEventListener):
    def handle_user_is_created_event(sealf, event: UserIsCreatedEvent):
        """
        Prints a message for the UserIsCreatedEvent
        :param event:
        :return:
        """
        print("Hello {name}".format(name=event.name))

