
from eventsourcing.abstract import BaseEvent
from eventsourcing.rabbitmq import RabbitMQEventsPublisher, RabbitMQConnection
from eventsourcing.in_memory import InMemoryEventBus

from eventsourcing.serializer.marshmallow import MarshmallowJSONSerializer
from events import UserIsCreatedSchema, UserIsRemovedSchema, UserIsRemovedEvent, UserIsCreatedEvent
from marshmallow_oneofschema import OneOfSchema

class UserEventSchemas(OneOfSchema):
    type_field = "messageType"
    type_schemas = {
        'UserIsCreatedEvent': UserIsCreatedSchema,
        'UserIsRemovedEvent': UserIsRemovedSchema
    }

if __name__ == '__main__':
    in_memory_bus = InMemoryEventBus()

    # Connect to rabbitmq and add an in-memory event listener which publishes all events to rabbitmq
    rabbitmq_connection = RabbitMQConnection("amqp://rabbitmq:rabbitmq@localhost:5672/")

    serializer = MarshmallowJSONSerializer(schema=UserEventSchemas)
    rabbitmq_events_publisher = RabbitMQEventsPublisher(rabbitmq_connection, serializer)
    in_memory_bus.subscribe(rabbitmq_events_publisher)

    # publish to in-memory rabbitmq_bus (which is also copied to RabbitMQ). This enabled you to handle some events synchronous while others can
    # be handled in an asynchronous way
    in_memory_bus.publish(UserIsRemovedEvent(user_id='1337'))
    in_memory_bus.publish(UserIsCreatedEvent(name='Lars'))