from eventsourcing.rabbitmq import RabbitMQConnection
from eventsourcing.abstract import AbstractSerializer

from producer import UserIsRemovedEvent, UserIsCreatedEvent, UserEventSchemas
from eventsourcing.rabbitmq import RabbitMQEventsListener
from eventsourcing.serializer.marshmallow import MarshmallowJSONSerializer

class PrintEventEventsListener(RabbitMQEventsListener):
    def __init__(self, serializer: AbstractSerializer):
        RabbitMQEventsListener.__init__(self, serializer)
    def handle_user_is_created_event(self, event: UserIsCreatedEvent):
        print("Hello {name}".format(name=event.name))

    def handle_user_is_removed_event(self, event: UserIsRemovedEvent):
        print("Bye {name}".format(name=event.user_id))

if __name__ == '__main__':
    rabbitmq_bus = RabbitMQConnection("amqp://rabbitmq:rabbitmq@localhost:5672/")
    serializer = MarshmallowJSONSerializer(schema=UserEventSchemas)
    event_listener = PrintEventEventsListener(serializer=serializer)
    rabbitmq_bus.subscribe(event_listener)

    # blocking
    rabbitmq_bus.consume()
