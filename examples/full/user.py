from eventsourcing.abstract import AggregateRoot
from events import UserChangedNameEvent, UserIsRemovedEvent, UserIsCreatedEvent
class User(AggregateRoot):
    @staticmethod
    def create(user_id, name):
        """
        Spawns a new user with a given user_id and name
        :param user_id: str
        :param name: str
        :return: User
        """
        user = User()
        user.apply(UserIsCreatedEvent(user_id=user_id, name=name, status='active'))
        return user

    @property
    def aggregate_id(self):
        """
        Mandatory function to determine the id of the aggregate
        :return:
        """
        return self.user_id

    def remove(self):
        """
        Removes the current aggregate by setting its status to removed
        :return: self
        """
        self.apply(UserIsRemovedEvent(self.user_id, self.name, status='removed'))

        return self

    def change_name(self, new_name: str):
        """
        Changes the name of the aggregate and fires an UserChangedNameEvent event
        :param new_name: str
        :return: self
        """
        self.apply(UserChangedNameEvent(self.user_id, old_name=self.name, new_name=new_name))

        return self

    def apply_user_is_created_event(self, event: UserIsCreatedEvent):
        """
        Set the correct properties for the newly created user
        :param event: UserIsCreatedEvent
        :return:
        """
        self.user_id = event.user_id
        self.name = event.name
        self.status = event.status

    def apply_user_is_removed_event(self, event: UserIsRemovedEvent):
        """
        Apply the change to the user his status
        :param event: UserIsRemovedEvent
        :return:
        """
        self.status = event.status

    def apply_user_changed_name_event(self, event: UserChangedNameEvent):
        """
        Apply the name change
        :param event: UserChangedNameEvent
        :return:
        """
        self.name = event.new_name

