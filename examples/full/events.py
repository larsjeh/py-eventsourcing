
from eventsourcing.abstract import BaseEvent

class UserIsRemovedEvent(BaseEvent):
    """
    Event which marks the removal of an user
    """

    def __init__(self, user_id: str, name: str, status: str):
        """
        Initializes event
        :param user_id: str
        :param name: str
        :param status: str
        """
        self.user_id = user_id
        self.name = name
        self.status = status

class UserIsCreatedEvent(BaseEvent):
    """
    Event which marks the creation of an user
    """

    def __init__(self, user_id: str, name: str, status: str):
        """
        Initializes event
        :param user_id: str
        :param name: str
        :param status: str
        """
        self.user_id = user_id
        self.name = name
        self.status = status

class UserChangedNameEvent(BaseEvent):
    """
    Event which marks the name change of an user
    """

    def __init__(self, user_id: str, old_name: str, new_name: str):
        self.user_id = user_id
        self.new_name = new_name
        self.old_name = old_name
