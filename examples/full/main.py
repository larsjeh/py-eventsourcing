import uuid

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from eventsourcing.abstract import EventSourcingRepository
from eventsourcing.in_memory import InMemoryEventBus
from eventsourcing.in_memory import InMemoryCommandBus
from eventsourcing.eventstore.sqlalchemy import SqlAlchemyEventstore
from eventsourcing.eventstore.entity import Base

from commands import ChangeNameCommand, CreateUserCommand
from print_event_listener import PrintEventListener
from user_command_handler import UserCommandHandler
from user import User


if __name__ == '__main__':
    # Create in-memory eventbus
    event_bus = InMemoryEventBus()

    # add listener so we can see everything is working
    print_event_listener = PrintEventListener()
    event_bus.subscribe(print_event_listener)

    # Create necessary db connection to configure a mysql event store
    engine = create_engine('mysql://root:eventstore@127.0.0.1:3306/eventstore')
    Base.metadata.create_all(engine)
    db_session = scoped_session(sessionmaker(bind=engine))

    event_store = SqlAlchemyEventstore(db_session)

    # create command handler and subscribe it to the command rabbitmq_bus
    eventsourced_repository = EventSourcingRepository(event_store, event_bus, User)
    user_command_handler = UserCommandHandler(eventsourced_repository)

    command_bus = InMemoryCommandBus()
    command_bus.subscribe(user_command_handler)

    # create and publish commands
    create_command = CreateUserCommand(username='another')
    change_command = ChangeNameCommand(user_id="48074ebc-1463-4732-8a54-b32d05ca0ed1", name="another-new-name")

    command_bus.publish(create_command)
    command_bus.publish(change_command)
