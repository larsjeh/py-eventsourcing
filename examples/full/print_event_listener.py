from eventsourcing.in_memory import InMemoryEventListener
from events import UserIsCreatedEvent, UserIsRemovedEvent, UserChangedNameEvent

class PrintEventListener(InMemoryEventListener):
    def handle_user_is_created_event(sealf, event: UserIsCreatedEvent):
        """
        Prints a message for the UserIsCreatedEvent
        :param event:
        :return:
        """
        print("Hello {name}".format(name=event.name))

    def handle_user_is_removed_event(self, event: UserIsRemovedEvent):
        """
        Prints a message for the UserIsRemovedEvent
        :param event:
        :return:
        """
        print("Bye {name}".format(name=event.name))

    def handle_user_changed_name_event(self, event: UserChangedNameEvent):
        """
        Prints a message for the UserChangedNameEvent
        :param event:
        :return:
        """
        print("Changing name to {name} from {old_name} for user_id: {user_id}".format(name=event.new_name, old_name=event.old_name, user_id=event.user_id))
