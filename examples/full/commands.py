from eventsourcing.abstract import Command
class CreateUserCommand(Command):
    def __init__(self, username: str):
        self.username = username


class ChangeNameCommand(Command):
    def __init__(self, user_id: str, name: str):
        self.user_id = user_id
        self.name = name

class RemoveUserCommand(Command):
    def __init__(self, user_id: str):
        self.user_id = user_id
