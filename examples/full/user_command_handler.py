from eventsourcing.abstract import EventSourcingRepository
from eventsourcing.in_memory import InMemoryCommandHandler
from commands import CreateUserCommand, ChangeNameCommand, RemoveUserCommand
from user import User
import uuid

class UserCommandHandler(InMemoryCommandHandler):
    def __init__(self, repository: EventSourcingRepository):
        # create event sourcing repository containing event store and event rabbitmq_bus
        self._repository = repository


    def handle_create_user_command(self, command: CreateUserCommand):
        """
        Handles the create user command
        :param command:
        :return:
        """
        user_id = uuid.uuid4()
        user = User.create(str(user_id), command.username)

        # save aggregate to repository
        self._repository.save(user)

    def handle_change_name_command(self, command: ChangeNameCommand):
        """
        Handles the change names command
        :param command:
        :return:
        """
        user = self._repository.load(command.user_id)

        if not isinstance(user, User):
            print("Unable to find user with user_id:{}".format(command.user_id))
            return

        if user.status == 'removed':
            print("Cannot change the name of an user with status removed for user_id:".format(command.user_id))
            return

        # change name from user
        user = user.change_name(new_name=command.name)

        # save aggregate to repository
        self._repository.save(user)

    def handle_remove_user_command(self, command: RemoveUserCommand):
        """
        Handles the removal of an user command
        :param command:
        :return:
        """
        user = self._repository.load(command.user_id)

        if not isinstance(user, User):
            print("Unable to find user with user_id:{}".format(command.user_id))
            return

        # remove user
        user.remove()

        self._repository.save(user)