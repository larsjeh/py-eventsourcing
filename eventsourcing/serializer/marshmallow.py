
from eventsourcing.abstract import AbstractSerializer
from eventsourcing.abstract import BaseEvent
import logging



class SerializationException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args)
        self.validation_errors = kwargs.get("validation_errors", {})

class MarshmallowJSONSerializer(AbstractSerializer):
    def __init__(self, schema):
        self.schema = schema
        self.logger = logging.getLogger("eventsourcing")

    def serialize(self, event: BaseEvent) -> str:
        data, errors = self.schema().dumps(event)
        if not errors:
            return data
        else:
            self.logger.exception(errors)
            raise SerializationException("Error serializing event", validation_errors=errors)

    def deserialize(self, data: str) -> BaseEvent:
        data, errors = self.schema().loads(data)
        if not errors:
            return data
        else:
            self.logger.exception(errors)
            raise SerializationException("Error deserializing kinesis event", validation_errors=errors)
