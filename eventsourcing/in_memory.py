import eventsourcing.helpers as helpers
from eventsourcing.abstract import Command, CommandHandler

from eventsourcing.abstract import EventListener
import logging

from eventsourcing.abstract import EventBus
from eventsourcing.abstract import BaseEvent
import logging


class InMemoryCommandHandler(CommandHandler):
    def handle(self, command: Command):
        """
        Calls a function based on the event name
        :param command: Command
        :return: None
        """
        underscored_command_name = helpers.camelcase_to_underscore(command.__class__.__name__)
        method_name = "handle_{command_name}".format(command_name=underscored_command_name)

        try:
            handle_method = getattr(self, method_name)
            result = handle_method(command)
        except AttributeError as e:
            print(e)
            pass
        except Exception as e:
            raise (e)


class InMemoryCommandBus(EventBus):
    handlers = []

    def publish(self, command: Command):
        """
        Calls all the subscribed handlers
        :param event:
        :return: bool for success
        """
        try:
            for handler in self.handlers:
                handler.handle(command)
        except Exception as e:
            raise (e)

    def subscribe(self, handler: CommandHandler):
        """
        Subscribe a handler to the event rabbitmq_bus
        :param handler:
        :return:
        """
        self.handlers.append(handler)


class InMemoryEventBus(EventBus):
    def __init__(self):
        self.handlers = []
        self.logger = logging.getLogger("eventsourcing")

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, value):
        self._logger = value

    @property
    def handlers(self) -> list:
        return self._handlers

    @handlers.setter
    def handlers(self, value):
        self._handlers = value

    def publish(self, event: BaseEvent) -> bool:
        """
        Calls all the subscribed handlers
        :param event:
        :return: bool for success
        """
        try:
            for handler in self.handlers:
                handler.handle(event)
                self.logger.info(
                    "Published event ({event_name}) to handler ({handler})".format(event_name=event.__class__.__name__, handler=handler.__class__.__name__),
                    extra={'event': event.__dict__})
        except Exception as e:
            self.logger.exception(e, extra={'event': event.__dict__})

    def subscribe(self, handler):
        """
        Subscribe a handler to the event rabbitmq_bus
        :param handler:
        :return:
        """
        self.handlers.append(handler)


class InMemoryEventListener(EventListener):
    def __init__(self):
        self.logger = logging.getLogger("eventsourcing")

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, value):
        self._logger = value

    def handle(self, event: BaseEvent):
        """
        Calls a function based on the event name
        :param event:
        :return:
        """
        underscored_event_name = helpers.camelcase_to_underscore(event.__class__.__name__)
        method_name = "handle_{event_name}".format(event_name=underscored_event_name)

        try:
            handle_method = getattr(self, method_name, None)
            if handle_method is None:
                # no method for this class
                return

            self.logger.info('Handling {method_name} for {event_name}'.format(method_name=method_name,
                                                                              event_name=event.__class__.__name__),
                             extra={'payload': event.__dict__})
            result = handle_method(event)
        except Exception as e:
            import traceback
            self.logger.error(
                "Error execution function for event: {event_name} and {error_type}: {error}. Traceback {q} ".format(
                    event_name=event.__class__.__name__,
                    error_type=e.__class__.__name__,
                    error=e,
                    q=traceback.format_exc()), extra={'event': event.__dict__})
