from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, UniqueConstraint, DateTime
import datetime
import pytz

Base = declarative_base()

class Event(Base):
    __tablename__ = 'eventstore'
    aggregate_id = Column(String(255), autoincrement=False, primary_key=True)
    playhead = Column(Integer, autoincrement=False, primary_key=True)
    event_name = Column(String(255))
    payload = Column(String(255))

    created_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, aggregate_id, playhead, event_name, payload):
        self.aggregate_id = aggregate_id
        self.playhead = playhead
        self.event_name = event_name
        self.payload = payload