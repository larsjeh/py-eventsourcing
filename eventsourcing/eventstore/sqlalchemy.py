from eventsourcing.abstract import EventStore, BaseEvent
from eventsourcing.eventstore.entity import Event
from eventsourcing.abstract import BaseEvent
import json, logging
from collections import namedtuple
from typing import Union


class SqlAlchemyEventstore(EventStore):
    def __init__(self, session):
        self._session = session
        self.logger = logging.getLogger("eventsourcing")

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, value):
        self._logger = value

    def handle(self, event: BaseEvent):
        """
        Persist the actual event to the database
        :param event:
        :return:
        """

        # Copy to make sure object is not passed by reference or something
        event_dict = event.__dict__.copy()

        event_dict = self._clean_event_dict(event_dict)
        payload = json.dumps(event_dict)

        event_entity = Event(event.aggregate_id, event.playhead, event.__class__.__name__, payload)
        session = self._session
        try:
            session.add(event_entity)
            session.commit()
        except:
            session.rollback()

        self.logger.debug("Event saved to database: {event}".format(event=event.__dict__))


    def load_event_stream(self, aggregate_id: str) -> Union[list, None]:
        """
        Load an entity by its aggregate_id
        :param aggregate_id: string
        :return: returns a list containing all the events for a certain aggregate
        """
        session = self._session
        events = session.query(Event).filter(Event.aggregate_id == aggregate_id).order_by(Event.playhead.asc()).all()


        if len(events) == 0:
            # No events are found for this aggregate
            return None

        return self._create_events_from_database(events)


    def _create_events_from_database(self, events) -> list:
        """
        Create Events from the payload in the database
        :param events:
        :return:
        """
        deserialized_events = []
        for event in events:
            event_dict = event.__dict__

            # Create dictionary with the necessary information to run the events
            payload = json.loads(event_dict['payload'])

            payload['aggregate_id'] = event_dict['aggregate_id']
            payload['created_at'] = event.created_at
            serialized_event = namedtuple(event_dict['event_name'], payload.keys())(*payload.values())

            deserialized_events.append(serialized_event)

        return deserialized_events

    def _clean_event_dict(self, event_dict: dict) -> dict:
        """
        Clean dict before insering it in the database
        :param event_dict:
        :return:
        """
        del event_dict['_aggregate_id']
        del event_dict['_playhead']

        return event_dict
