from eventsourcing.abstract import BaseEvent, AbstractSerializer
from eventsourcing.in_memory import InMemoryEventListener
import logging
from kiner.producer import KinesisProducer


class KinesisEventPublisher(InMemoryEventListener):

    def __init__(self, kinesis_producer: KinesisProducer, serializer: AbstractSerializer):
        InMemoryEventListener.__init__(self)
        self._kinesis_producer = kinesis_producer
        self._serializer = serializer

    @property
    def serializer(self):
        return self._serializer

    @serializer.setter
    def serializer(self, value):
        self._serializer = value

    @property
    def kinesis_producer(self):
        return self._kinesis_producer

    @kinesis_producer.setter
    def kinesis_producer(self, value):
        self._kinesis_producer = value

    def handle(self, event: BaseEvent):

        logger = logging.getLogger('eventsourcing')
        logger.debug("Publishing event to kinesis", extra={"event": event})

        self.kinesis_producer.put_record(self.serializer.serialize(event))