import re
def camelcase_to_underscore(name):
    """
    Changes the CamelCase string to an under_score variant
    :param name: str
    :return: str
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)

    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()