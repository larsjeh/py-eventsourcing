from eventsourcing.abstract import BaseEvent, EventBus
from eventsourcing.listener.in_memory import InMemoryEventListener
from eventsourcing.abstract import EventListener, AbstractSerializer
from eventsourcing.helpers import camelcase_to_underscore
import pika

class RabbitMQEventsListener(EventListener):
    def __init__(self, serializer : AbstractSerializer):
        self._serializer = serializer

    @property
    def serializer(self):
        return self._serializer

    @serializer.setter
    def serializer(self, value):
        self._serializer = value

    def callback_func(self, channel, method, properties, body):
        self.handle(self._serializer.deserialize(body))

    def handle(self, event: BaseEvent):
        """
        Calls a function based on the event name
        :param event: BaseEvent
        :return:
        """
        underscored_event_name = camelcase_to_underscore(event.__class__.__name__)
        method_name = "handle_{event_name}".format(event_name=underscored_event_name)

        try:
            handle_method = getattr(self, method_name)
            result = handle_method(event)
        except AttributeError as e:
            # function does not exist, skip
            pass

class RabbitMQConnection(EventBus):
    def __init__(self, url: str, exchange_name='events-exchange', durable=True):
        parameters = pika.URLParameters(url)
        self._connection = pika.BlockingConnection(parameters)
        self._channel = self._connection.channel()
        self._channel.exchange_declare(exchange=exchange_name, exchange_type='fanout', durable=durable)
        self.exchange_name = exchange_name

    def publish(self, serialized_event: str):
        """
        Publish an event to the rabbitMQ event broker
        :param event: BaseEvent
        :return: bool for success
        """
        return self._channel.basic_publish(exchange=self.exchange_name, routing_key='', body=serialized_event)

    def consume(self):
        """
        Consume from queue
        :return:
        """
        self._channel.start_consuming()

    def subscribe(self, handler: RabbitMQEventsListener):
        """
        Subscribe event listener and start listening to the events received from the exchange
        :param handler:
        :return:
        """

        queue_name = handler.__class__.__name__

        # declare queue and bind it to the correct exchange
        result = self._channel.queue_declare(queue_name)
        queue_name = result.method.queue
        self._channel.queue_bind(exchange=self.exchange_name, queue=queue_name)

        # setup consuming
        self._channel.basic_consume(handler.callback_func,
                                    queue=queue_name,
                                    no_ack=True)


class RabbitMQEventsPublisher(InMemoryEventListener):
    def __init__(self, rabbitmq_connection: RabbitMQConnection, serializer: AbstractSerializer) :
        self._rabbitmq_connection = rabbitmq_connection
        self._serializer = serializer

    @property
    def serializer(self):
        return self._serializer

    @serializer.setter
    def serializer(self, value):
        self._serializer = value

    @property
    def rabbitmq_connection(self):
        return self._rabbitmq_connection

    @rabbitmq_connection.setter
    def rabbitmq_connection(self, value):
        self._rabbitmq_connection = value

    def handle(self, event: BaseEvent):
        """
        Override method from original event listener. This function puts all the events from the in-memory rabbitmq_bus on the rabbit-mq rabbitmq_bus
        as well
        :param event: BaseEvent
        :return:
        """
        self._rabbitmq_connection.publish(self.serializer.serialize(event))
