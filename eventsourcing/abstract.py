import abc
import eventsourcing.helpers as helpers
import logging
from typing import Union, List
from collections import namedtuple
import traceback


class BaseEvent(object):
    @property
    def version(self):
        """
        Set standard to the first version, can be overwritten if necessary
        :return:
        """
        return 1

    @property
    def playhead(self):
        return self._playhead

    @playhead.setter
    def playhead(self, value):
        self._playhead = value

    @property
    def aggregate_id(self):
        return self._aggregate_id

    @aggregate_id.setter
    def aggregate_id(self, value):
        self._aggregate_id = value


class EventListener(abc.ABC):
    @abc.abstractmethod
    def handle(self, event: BaseEvent):
        raise NotImplementedError


class EventBus(abc.ABC):
    @abc.abstractmethod
    def publish(self, event: BaseEvent):
        """
        Publish an event
        :param event:
        :return:
        """
        raise NotImplementedError

    @abc.abstractmethod
    def subscribe(self, handler: EventListener):
        raise NotImplementedError


class EventStore(EventListener):
    @abc.abstractmethod
    def load_event_stream(self, aggregate_id: str):
        raise NotImplementedError

    @abc.abstractmethod
    def handle(self, event: BaseEvent):
        raise NotImplementedError


class AggregateRoot(object):
    def __init__(self):
        self.uncommitted_events = []
        self.playhead = 1
        self.logger = logging.getLogger(__name__)

    def handle(self, event):
        underscored_event_name = helpers.camelcase_to_underscore(event.__class__.__name__)
        method_name = "apply_{event_name}".format(event_name=underscored_event_name)

        try:
            handle_method = getattr(self, method_name)
            result = handle_method(event)
            self.logger.debug(
                "Applying method {method_name} with event {event}".format(method_name=method_name,
                                                                          aggregate_root=self.__class__.__name__,
                                                                          event=event.__class__.__name__
                                                                          ), extra={'event': event.__dict__})
        except AttributeError:
            # method does not exist
            pass
        except Exception as e:
            print(e)
            self.logger.error("Error execution function for event: {event_name} {error_type}: {error} ".format(
                event_name=event.__class__.__name__,
                error_type=e.__class__.__name__,
                error=e), extra={'event': event.__dict__})
            self.logger.error(traceback.format_exc())

    def handle_recursively(self, event: BaseEvent, append_to_uncommited=True):
        """
        Handle all child entites
        :param event:
        :param append_to_uncommited:
        :return:
        """
        self.handle(event)

        for child_entity in self.child_entities:
            child_entity.register_aggregate_root(self)
            child_entity.handle_recursively(event)

        if append_to_uncommited:
            event.aggregate_id = self.aggregate_id
            event.playhead = self.playhead
            self.uncommitted_events.append(event)

        self.playhead += 1

    def apply(self, event: BaseEvent, append_to_uncommited=True):
        """
        Apply event to the aggregate root. The events are saved in the uncommitted events list to be processed on save
        :param event:
        :return:
        """
        self.handle_recursively(event, append_to_uncommited)

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, value):
        self._logger = value

    @property
    def uncommitted_events(self) -> list:
        return self._uncommitted_events

    @uncommitted_events.setter
    def uncommitted_events(self, value):
        self._uncommitted_events = value

    @property
    @abc.abstractmethod
    def aggregate_id(self):
        raise NotImplementedError

    @property
    def playhead(self):
        return self._playhead

    @playhead.setter
    def playhead(self, value):
        self._playhead = value

    @property
    def child_entities(self):
        """
        Overwrite this function if used
        :return:
        """
        return []


class EventSourcedEntity(abc.ABC):
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    @property
    def child_entities(self):
        return []

    def handle(self, event):
        underscored_event_name = helpers.camelcase_to_underscore(event.__class__.__name__)
        method_name = "apply_{event_name}".format(event_name=underscored_event_name)

        try:
            handle_method = getattr(self, method_name)
            result = handle_method(event)
            self.logger.debug(
                "Applying method {method_name} with event {event}".format(method_name=method_name,
                                                                          aggregate_root=self.__class__.__name__,
                                                                          event=event))
        except AttributeError:
            # method does not exist
            pass
        except Exception as e:
            self.logger.error("Error execution function for event: {event_name} with {error_type}: {error} ".format(
                event_name=event.__class__.__name__,
                error_type=e.__class__.__name__,
                error=e), extra={'event': event.__dict__})

    def handle_recursively(self, event: BaseEvent):
        """
        Handle all child entites
        :param event:
        :param append_to_uncommited:
        :return:
        """
        self.handle(event)

        for child_entity in self.child_entities:
            child_entity.register_aggregate_root(self)
            child_entity.handle_recursively(event)

    def apply(self, event: BaseEvent):
        """
        Apply event to the aggregate root. The events are saved in the uncommitted events list to be processed on save
        :param event:
        :return:
        """
        self.aggregate_root.handle_recursively(event)

    def register_aggregate_root(self, aggregate: AggregateRoot):
        self.aggregate_root = aggregate


class AbstractSerializer(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def serialize(self, event: BaseEvent) -> str:
        pass

    @abc.abstractmethod
    def deserialize(self, data):
        pass


class Command(object):
    pass


class CommandHandler(abc.ABC):
    @abc.abstractmethod
    def handle(self, command: Command):
        raise NotImplementedError


class EventSourcingRepository(object):
    def __init__(self, event_store: EventStore, event_bus: EventBus, aggregate_cls: type, in_memory_cache=None):
        """
        Be careful with using the in memory cache because this can give weird behaviour when running in a redundant setup
        :param event_store:
        :param event_bus:
        :param aggregate_cls:
        :param in_memory_cache:
        """
        self._event_store = event_store
        self._event_bus = event_bus
        self._aggregate_cls = aggregate_cls
        self.logger = logging.getLogger(__name__)

        self.cache = in_memory_cache

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, value):
        self._logger = value

    def save(self, aggregate: AggregateRoot) -> AggregateRoot:
        """
        Save an aggregate its events and publish them on the event rabbitmq_bus
        :param aggregate:
        :return:
        """
        events = aggregate.uncommitted_events

        for event in events:
            self._event_store.handle(event)
            self._event_bus.publish(event)

        aggregate.uncommitted_events = []
        self.logger.debug("Aggregate saved: {aggregate_id}".format(aggregate_id=aggregate.__dict__))

        if self.cache is not None:
            self.cache[aggregate.aggregate_id] = aggregate

        return aggregate

    def _create_aggregate_by_events(self, events) -> Union[AggregateRoot, None]:
        """
        Recreate an an aggregate by calculating all its events
        :param events:
        :return:
        """
        aggregate_class = self._aggregate_cls()
        if not events:
            # no events. aggregate does not exist
            return None

        for event in events:
            aggregate_class.apply(event, append_to_uncommited=False)

        return aggregate_class

    def load(self, id: str) -> Union[AggregateRoot, None]:
        """
        Load an aggregate by its ID
        :param id:
        :return:
        """
        if self.cache is not None and id in self.cache:
            return self.cache[id]

        aggregate_events = self._event_store.load_event_stream(id)
        aggregate = self._create_aggregate_by_events(aggregate_events)

        if self.cache is not None:
            self.cache[id] = aggregate

        return aggregate
